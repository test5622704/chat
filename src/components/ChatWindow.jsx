import React from 'react';
import './ChatWindow.css';
import Chat from './Chat'; // Chat importieren

function ChatWindow() {
  return (
    <div className="chat-window">
      <h2>Chat</h2>
      <div className="chat-container">
        <Chat /> {/* Chat-Komponente anzeigen */}
      </div>
    </div>
  );
}

export default ChatWindow;
