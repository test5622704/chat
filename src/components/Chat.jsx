import React, { useState, useEffect } from 'react';
import './Chat.css';
import bobAvatar from './Bob.jpg'; // Bild für Bob
import aliceAvatar from './Alice.jpg'; // Bild für Alice
import johnAvatar from './John.jpg'; // Bild für John

function Chat() {
  const [messages, setMessages] = useState([]);
  const [input, setInput] = useState('');
  const users = ['John', 'Alice', 'Bob']; // Liste der Benutzernamen
  const userAvatars = { Bob: bobAvatar, Alice: aliceAvatar, John: johnAvatar}; // Profilbilder für Bob und Alice

  const getRandomUser = () => {
    const randomIndex = Math.floor(Math.random() * users.length);
    return users[randomIndex];
  };

  const getCurrentTime = () => {
    const now = new Date();
    const hours = now.getHours().toString().padStart(2, '0');
    const minutes = now.getMinutes().toString().padStart(2, '0');
    return `${hours}:${minutes}`;
  };

  const handleMessageChange = (e) => {
    setInput(e.target.value);
  };

  const handleMessageSubmit = (e) => {
    e.preventDefault();
    if (input.trim() !== '') {
      const newMessage = {
        text: input,
        timestamp: getCurrentTime(),
        sender: getRandomUser(),
      };
      setMessages([...messages, newMessage]);
      setInput('');
    }
  };

  useEffect(() => {
    const interval = setInterval(() => {
      const updatedMessages = messages.map(message => {
        if (message.timestamp === 'now') {
          return {
            ...message,
            timestamp: getCurrentTime()
          };
        }
        return message;
      });
      setMessages(updatedMessages);
    }, 60000); // Aktualisieren alle 60 Sekunden
    return () => clearInterval(interval);
  }, [messages]);

  return (
    <div className="chat-container">
      <div className="message-list">
        {messages.map((message, index) => (
          <div key={index} className={`message ${message.sender}`}>
            <div className="avatar-container">
              <img src={userAvatars[message.sender]} alt="" className="avatar" />
              <p className="user-name">{message.sender}</p>
            </div>
            <div className="message-content">
              <p className="text">{message.text}</p>
              <span className="timestamp">{message.timestamp}</span>
            </div>
          </div>
        ))}
      </div>
      <form onSubmit={handleMessageSubmit} className="input-form">
        <input
          type="text"
          value={input}
          onChange={handleMessageChange}
          placeholder="Type your message..."
          className="message-input"
        />
        <button type="submit" className="send-button">Send</button>
      </form>
    </div>
  );
}

export default Chat;
