import React from 'react';
import './Message.css';

function Message({ text, timestamp }) {
  return (
    <div className="message">
      <p>{text}</p>
      <span className="timestamp">{timestamp}</span>
    </div>
  );
}

export default Message;
